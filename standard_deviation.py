
class StandardDeviation:
    def __init__(self, num):
        self.num = num
    def my_n(self):
        return len(self.num)-1
    def mymean(self):
        return sum(self.num)/self.my_n()
    def mysubeach(self):
        return [x - self.mymean() for x in self.num]
    def mysqrdiff(self):
        return [x**2 for x in self.mysubeach()]
    def mysumeach(self):
        return sum(self.mysqrdiff())
    def mydivtotal(self):
        return self.mysumeach()/self.my_n()
    def mysqrtotal(self):
        return self.mydivtotal() ** 0.5
    def __str__(self):
        print("n: ", self.my_n())
        print("mean: ", self.mymean())
        print("subtruct to each: ", self.mysubeach())
        print("square root each: ", self.mysqrdiff())
        print("sum square rooted: ", self.mysumeach())
        print("divided sum rooted: ", self.mydivtotal())
        print("variance: ", self.mysqrtotal())
        
if __name__ == '__main__':
    dataset = [56,76,89,23,45,56,78,90]
    main = StandardDeviation(dataset)
    print(main.__str__())